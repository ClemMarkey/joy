from flask import redirect, render_template

class DeplacementController():
    def fetch_deplacement(self, model):
        return render_template('deplacements.html', data = model.fetch_dep())

    def vue_add_deplacement(self, infirmieres, patients):
        return render_template('formulaire_deplacement.html', infirmieres=infirmieres.fetch_all(), patients=patients.fetch_all())

    def add_deplacement(self, model, deplacement):
        return model.addDeplacement(deplacement)

    def delete_deplacement(self, model, id):
        model.deleteDeplacement(id)
        return redirect('http://localhost:5000/deplacements')

    def vue_update_deplacement(self, data, infirmieres, patients):
        return render_template('update_deplacement_formulaire.html', data=data, infirmieres=infirmieres.fetch_all(), patients=patients.fetch_all())

    def update_deplacement(self, model, data):
        model.updateDeplacement(data)
        return redirect('http://localhost:5000/deplacements')