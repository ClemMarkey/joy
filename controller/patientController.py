from flask import render_template, redirect

class PatientController():
    def fetch_patients(self, model):
        return render_template('patients.html', data=model.fetch_all())

    def vue_add_patient(self, infirmieres):
        return render_template('formulaire_patient.html', infirmieres=infirmieres.fetch_all())

    def add_pat(self, model, data):
        return model.addPatient(data)

    def del_pat(self, model, id):
        model.delPatient(id)
        return redirect('http://localhost:5000/patients/')

    def vue_upd_pat(self, data):
        return render_template('upd_pat_form.html', data=data)

    def upd_pat(self, modelpat, modeladr, data):
        modelpat.updPatient(data)
        modeladr.updAdresse(data)
        return redirect('http://localhost:5000/patients/')