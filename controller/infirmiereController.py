from flask import render_template, redirect

class InfirmiereController():
    def fetch_infirmiere(self, model):
        return render_template('infirmieres.html', data=model.fetch_all())

    def vue_add_infirmiere(self):
        return render_template('formulaire.html')

    def add_inf(self, model, inf, adresseId):
        return model.addInfirmiere(inf, adresseId)
    
    def supr_infirmiere(self, model, id):
        model.supr_infirmiere(id)
        return redirect('http://localhost:5000/infirmieres')
    
    def vue_update_inf(self, data):
        return render_template('update_inf_formulaire.html', data=data)
    
    def update_inf(self, modelinf, modeladr, data):
        modelinf.update_infirmiere(data)
        modeladr.updAdresse(data)
        return redirect('http://localhost:5000/infirmieres')