from controller.adresseController import AdresseController
from controller.deplacementController import DeplacementController
from model.deplacement import DeplacementModel
from flask import Flask, request, render_template, redirect
from controller.patientController import PatientController
from model.patient import PatientModel
from controller.infirmiereController import InfirmiereController
from model.infirmiere import InfirmiereModel
from model.adresse import AdresseModel

app = Flask(__name__, static_folder='templates/assets')

infController = InfirmiereController()
infModel = InfirmiereModel()
patController = PatientController()
adrController = AdresseController()
patModel = PatientModel()
depController = DeplacementController()
depModel = DeplacementModel()
adrModel = AdresseModel()

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/infirmieres/')
def view_infirmieres():
    return infController.fetch_infirmiere(infModel)

@app.route('/patients/')
def view_patients():
    return patController.fetch_patients(patModel)

@app.route('/deplacements/')
def view_deplacements():
    return depController.fetch_deplacement(depModel)

@app.route('/formulaire/')
def formulaire_infirmiere():
    return infController.vue_add_infirmiere()

@app.route('/formulaire_patient/')
def formulaire_patient():
    return patController.vue_add_patient(infModel)

@app.route('/create_patient/', methods = ['POST', 'GET'])
def add_patient():
    formData = request.form
    patController.add_pat(patModel, formData)
    return redirect("http://localhost:5000/patients")

@app.route('/create_infirmiere/', methods = ['POST', 'GET'])
def add_infirmieres():
    formData = request.form
    adrController.add_adresse(adrModel, formData)
    infController.add_inf(infModel, formData, adrModel.getLast())
    return redirect("http://localhost:5000/infirmieres")

@app.route('/del_pat/<int:id>')
def del_pat(id):
    return patController.del_pat(patModel, id)

@app.route('/delete/<int:id>')
def del_inf(id):    
    return infController.supr_infirmiere(infModel, id)

@app.route('/upd_pat_form/')
def pat_form_update():    
    formData = request.args    
    return patController.vue_upd_pat(formData)

@app.route('/upd_pat/', methods = ['POST', 'GET'])
def update_pat():    
    formData = request.form
    print(formData)    
    return patController.upd_pat(patModel, adrModel, formData)

@app.route('/update_inf_formulaire/')
def inf_formulaire_update():
    formData = request.args    
    return infController.vue_update_inf(formData)
    
@app.route('/update_infirmiere/', methods = ['POST', 'GET'])
def update_infirmiere():    
    formData = request.form
    print(formData)
    return infController.update_inf(infModel, adrModel, formData)

@app.route('/formulaire_deplacement/')
def formulaire_deplacement():
    return depController.vue_add_deplacement(infModel, patModel)

@app.route('/create_deplacement/', methods = ['POST', 'GET'])
def add_deplacements():
    formData = request.form
    depController.add_deplacement(depModel, formData)
    return redirect('http://localhost:5000/deplacements')

@app.route('/delete_deplacement/<int:id>')
def del_dep(id):
    depController.delete_deplacement(depModel, id)
    return redirect('http://localhost:5000/deplacements')

@app.route('/update_deplacement_formulaire/')
def update_deplacement_formulaire():    
    formData = request.args    
    return depController.vue_update_deplacement(formData, infModel, patModel)

@app.route('/update_deplacement/', methods = ['POST','GET'])
def update_deplacement():
    formData = request.form
    print(formData)
    return depController.update_deplacement(depModel, formData)