from model.db import Connect

class AdresseModel:

    def __init__(self):
        self.conn = Connect.log()

    def addAdresse(self, data):
        self.conn.execute(f"""
            INSERT INTO adresse(numero, rue, cp,ville)
            VALUES(
                '{data.get('numero')}',
                '{data.get('rue')}',
                '{data.get('cp')}',
                '{data.get('ville')}')
            """)

    def updAdresse(self, data):
        self.conn.execute(
            f""" UPDATE adresse
            SET
            numero = '{data.get('numero')}',
            rue = '{data.get('rue')}',
            cp = '{data.get('cp')}',
            ville = '{data.get('ville')}'
            WHERE idadresse = '{data.get('idadresse')}'
            """)

    def getLast(self):
        self.conn.execute(
            f"""
            SELECT idadresse from adresse ORDER BY idadresse DESC LIMIT 1
            """)
        rows = self.conn.fetchall()
        return rows[0][0]