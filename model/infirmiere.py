from model.db import Connect

class InfirmiereModel():
    def __init__(self):
        self.conn = Connect.log()
    
    def fetch_all(self):
        self.conn.execute(""" SELECT idinfirmiere, identifiant_pro, nom, prenom, tel_pro, tel_perso, idadresse, numero, rue, cp, ville FROM infirmiere inner join adresse on (idadresse=adresse_idadresse) """)
        rows = self.conn.fetchall()
        return rows
        return rows

    def addInfirmiere(self, data, adresseId):
        self.conn.execute(
            f""" INSERT INTO infirmiere(identifiant_pro, nom, prenom, tel_pro, tel_perso, adresse_idadresse)
            VALUES(
            '{data.get('identifiant_pro')}',
            '{data.get('nom')}',
            '{data.get('prenom')}',
            '{data.get('tel_pro')}',
            '{data.get('tel_perso')}',
            '{adresseId}'
            )""")
    
    def supr_infirmiere(self, id):
        self.conn.execute(f"""DELETE FROM infirmiere WHERE idinfirmiere = {id}""")
    
    def update_infirmiere(self, data):
        self.conn.execute(
            f""" UPDATE infirmiere
            SET
            identifiant_pro = '{data.get('identifiant_pro')}',
            nom = '{data.get('nom')}',
            prenom = '{data.get('prenom')}',
            tel_pro = '{data.get('tel_pro')}',
            tel_perso = '{data.get('tel_perso')}'
            WHERE idinfirmiere = '{data.get('id')}'
            """)