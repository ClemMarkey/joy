from model.db import Connect

class PatientModel():
    def __init__(self):
        self.conn = Connect.log()
    
    def fetch_all(self):
        self.conn.execute("""
        SELECT idpatient, patient.nom, patient.prenom, date_de_naissance, genre,numero_secu, infirmiere.nom, infirmiere.prenom, adresse.idadresse, adresse.numero, adresse.rue, adresse.cp, adresse.ville
        FROM patient
        INNER JOIN infirmiere
        ON patient.infirmiere_idinfirmiere = infirmiere.idinfirmiere
        INNER JOIN adresse
        ON patient.adresse_idadresse = adresse.idadresse 
        """
        )
        rows = self.conn.fetchall()
        return rows

    def addPatient(self, data):
        self.conn.execute(f"""
            INSERT INTO patient(
                nom,
                prenom,
                date_de_naissance,
                genre,
                numero_secu,
                infirmiere_idinfirmiere
                )
            VALUES(
                '{data.get('nom')}',
                '{data.get('prenom')}',
                '{data.get('naissance')}',
                '{data.get('genre')}',
                '{data.get('secu')}',
                '{data.get('infirmiere_id')}')
            """)

    def delPatient(self, id):
        self.conn.execute(f"""DELETE FROM patient WHERE idpatient = {id}""")
    
    def updPatient(self, data):
        self.conn.execute(
            f""" UPDATE patient
            SET
            nom = '{data.get('nom')}',
            prenom = '{data.get('prenom')}',
            date_de_naissance = '{data.get('date_de_naissance')}',
            genre = '{data.get('genre')}',
            numero_secu = '{data.get('numero_secu')}'
            WHERE idpatient = '{data.get('id')}'
            """)