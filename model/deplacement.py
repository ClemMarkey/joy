from model.db import Connect

class DeplacementModel():
    def __init__(self):
        self.conn = Connect.log()

    def fetch_dep(self):
        self.conn.execute("""
        SELECT iddeplacement, infirmiere.nom, infirmiere.prenom, patient.nom, patient.prenom, date, cout
        FROM deplacement
        INNER JOIN infirmiere
        ON deplacement.infirmiere_idinfirmiere = infirmiere.idinfirmiere
        INNER JOIN patient
        ON deplacement.patient_idpatient = patient.idpatient
        """)
        rows = self.conn.fetchall()
        return rows

    def addDeplacement(self, data):
        self.conn.execute(
            f""" INSERT INTO deplacement (infirmiere_idinfirmiere, patient_idpatient, date, cout)
            VALUES (
            '{data.get('infirmiere_idinfirmiere')}',
            '{data.get('patient_idpatient')}',
            '{data.get('date')}',
            '{data.get('cout')}')
            """)

    def deleteDeplacement(self, id):
        self.conn.execute(
            f"""DELETE FROM deplacement WHERE iddeplacement = {id}"""
        )

    def updateDeplacement(self,data):
        self.conn.execute(
            f"""UPDATE deplacement
            SET 
            infirmiere_idinfirmiere = '{data.get('infirmiere_idinfirmiere')}',
            patient_idpatient = '{data.get('patient_idpatient')}',
            date = '{data.get('date')}',
            cout = '{data.get('cout')}'
            WHERE iddeplacement = '{data.get('id')}'
            """
        )